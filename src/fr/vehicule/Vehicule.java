package fr.vehicule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import fr.moteur.Moteur;
import fr.options.Option;

@SuppressWarnings("serial")
public class Vehicule implements Serializable {
	protected Double prix;
	protected String nom;
	protected List<Option> options = new ArrayList<Option>();
	protected Marque nomMarque;
	protected Moteur moteur;
	
	public Vehicule() {}
	
	public Vehicule(List<Option> options, Moteur moteur){
		this.options = options;
		this.moteur = moteur;
	}

	
	public String getNom(){
		return nom;
	}
	
	public void addOption(Option opt) {
		
		options.add(opt);
		
	}
	
	public Marque getMarque() {
		
		return nomMarque;
		
	}
	
	public List<Option> getOptions(){
		
		return options;
		
	}
	
	public Double getPrix() {
		
		return prix;
		
	}
	
	public Moteur getMoteur(){
		return moteur;
	}

	public void setMoteur(Moteur moteur) {
		this.moteur = moteur;
		
	}
	public String toString() {
		// La variable str nous permet d'afficher la chaine des options
		String str = " ";
		 for(Option op : this.getOptions()) {
			 str  = str+ " "+op.getTypeOption()+" ("+op.getPrix() +"�), ";
		 }
		 // utilisation d'un objet stream pour calculer le prix total de la voiture: les options + moteur
		 Stream<Option> ops = this.getOptions().stream();
		 Double sum = ops.filter(x -> x.getPrix() > 0).map(x -> x.getPrix()).reduce(0.0d, (x,y) -> x+y);
		 // � la sum on ajoute le prix du moteur
		 sum += moteur.getPrix();
		return "+ Voiture "+this.nomMarque+" : "+this.nom+" "+moteur.toString()+"["+ str+"] d'une valeur totale de "+sum+" �";
	}
}
