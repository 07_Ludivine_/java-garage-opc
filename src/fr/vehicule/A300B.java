package fr.vehicule;

public class A300B extends Vehicule {
	
	private static final long serialVersionUID = 2730666666476591458L;

	public A300B() {
		this.nomMarque = Marque.PIGEOT;
		this.nom = "A300B";
	}

}
