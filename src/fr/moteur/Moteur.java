package fr.moteur;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class Moteur implements Serializable {
	protected TypeMoteur type;
	protected String cylindre;
	protected Double prix;

	
	public Moteur(String cylindre,Double prix) {
		
		this.cylindre = cylindre;
		this.prix = prix;
		
	}

	public Double getPrix(){
		return prix;
	}

	public String toString(){
		return "Moteur "+type+"("+cylindre+", "+prix+" $)";
	}
}
