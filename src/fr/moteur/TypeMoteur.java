package fr.moteur;

public enum TypeMoteur {
	DIESEL,
	ESSENCE,
	HYBRIDE,
	ELECTRIQUE
}
