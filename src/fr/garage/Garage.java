package fr.garage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import fr.vehicule.Vehicule;

public class Garage {
	List<Vehicule> voitures = new ArrayList<Vehicule>();
	
	public Garage() {	

	}
	public void addVoiture(Vehicule voit) {
		ObjectOutputStream oos = null;
		// on ajoute l'objet Vehicule dans la liste des voitures
		voitures.add(voit);
		
		// on sauvegarde cet objet directement dans un fichier
		try {
			// Ouverture du fichier en �criture
			oos = new ObjectOutputStream(
					 new BufferedOutputStream(
							new FileOutputStream(
									new File("Garage.mgn"))));
			// apr�s ouverture du fichier, nous sauvegardons notre liste de Vehicule
			 oos.writeObject(voitures);
			
		} catch (IOException e) {e.printStackTrace();}
		finally {
			try {
				if(oos != null) oos.close();
				} catch (IOException e) {e.printStackTrace();}
			}
		
	} 
	@SuppressWarnings("unchecked")
	public String toString() {
		File f = new File("Garage.mgn");
		// si le fichier existe, on affiche les objets du garage
		if (f.exists()) {
			System.out.println("******************************************************");
			System.out.println("*              Garage OpenClassrooms                 *");
			System.out.println("******************************************************");
			// on ouvre le fichier en lecture
			ObjectInputStream ois;
			try {
				ois = new ObjectInputStream(
						new BufferedInputStream(
								new FileInputStream(f)));
				
					List<Vehicule> vl = (List<Vehicule>) ois.readObject();
					for(Vehicule ve : vl)
						System.out.println(ve.toString());
		        
				ois.close();
			} catch (IOException e) {e.printStackTrace();}
			  catch (ClassNotFoundException e) {e.printStackTrace();}
			
		} else {
			// System.err permet d'afficher sur la console d'eclispe le texte en rouge
			System.err.println("Aucune Voiture sauvegard�e ! ");
			System.out.println("******************************************************");
			System.out.println("*              Garage OpenClassrooms                 *");
			System.out.println("******************************************************");
		}
		return " ";
	}
	
	
}
