package fr.options;

@FunctionalInterface
public interface Option {	
	Double getPrix();
	// Methode par defaut 'getTypeOption' permet de definir le nom des options
	default String getTypeOption() {
		return getTypeOption();
	}
}
