package fr.options;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SiegeChauffant implements Option, Serializable {

	@Override
	public String getTypeOption(){
		return "Siege chauffant";
	}
	public Double getPrix() {
		return 175.00;
	}

}
