package fr.options;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Climatisation implements Option, Serializable {

	@Override
	public String getTypeOption(){
		return "Climatisation";
	}
	public Double getPrix() {
		return 600.00;
	}

}
