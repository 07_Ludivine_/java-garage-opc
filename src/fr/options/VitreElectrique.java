package fr.options;

import java.io.Serializable;

@SuppressWarnings("serial")
public class VitreElectrique implements Option, Serializable {

	@Override
	public String getTypeOption(){
		return "Vitre electrique";
	}
	public Double getPrix() {
		return 199.99;
	}

}
