package fr.options;

import java.io.Serializable;

@SuppressWarnings("serial")
public class BarreDeToit implements Option, Serializable {
	
	@Override
	public String getTypeOption(){
		return "Barre de toit";
	}
	public Double getPrix() {
		return 30.00;
	}

}
